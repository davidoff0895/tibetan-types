export interface IElementData {
  earthAndWatter: IElement
  fire: IElement
  wind: IElement
  eveningEarthAndWatter: IElement
}

interface IElement {
  allowedEat: IElementParams[]
  forbiddenEat: IElementParams[]
  recommendation: IRecommendation[]
}

interface IRecommendation extends IElementParams{
  list: string[]
}

interface IElementParams {
  text: string
  icon: string
}
