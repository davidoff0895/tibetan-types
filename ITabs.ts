export interface ITabs {
  allowedEat: ITab
  forbiddenEat: ITab
}

interface ITab {
  name: string
  description: string
}