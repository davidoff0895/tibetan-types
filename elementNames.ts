export const elementNames = {
  earthAndWatter: 'earthAndWatter',
  fire: 'fire',
  wind: 'wind',
  eveningEarthAndWatter: 'eveningEarthAndWatter',
  night: 'night',
}
