export interface IElementInfo {
  [key: string]: IElement
  earthAndWatter: IElement
  fire: IElement
  wind: IElement
  night: IElement
  eveningEarthAndWatter: IElement
}
export interface IElement {
  start: number
  end: number
  color: string
  title: string
  navigationTitle: string
}
